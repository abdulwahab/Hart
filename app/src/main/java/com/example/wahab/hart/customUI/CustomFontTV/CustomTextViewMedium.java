package com.example.wahab.hart.customUI.CustomFontTV;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.example.wahab.hart.customUI.CustomTV;
import com.example.wahab.hart.customUI.FontCache;

/**
 * Created by Wahab on 1/31/2018.
 */

public class CustomTextViewMedium extends CustomTV {
    public CustomTextViewMedium(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public CustomTextViewMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public CustomTextViewMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("WorkSans-Medium.ttf", context);
        setTypeface(customFont, Typeface.NORMAL);
//        setLinksClickable(true);
//        setAutoLinkMask(Linkify.WEB_URLS);
    }

}
