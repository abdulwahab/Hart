package com.example.wahab.hart.interfaces;

import android.os.Bundle;

/**
 * Created by incubasyss on 25/01/2018.
 */

public interface TopBarData {
    public void setData(String title, String description, boolean isNotify);

    public void setData(String title, String description, boolean isNotify, boolean isBack, boolean isOtherView, boolean isBulb, String tag);

    public void setData(String title, String description, boolean isNotify, boolean isBack, String tag);

    public void setData(String title, String description, boolean isNotify, boolean isBack, boolean isBulb, String tag);

    public void callTransection(Bundle bundle, int type);

    public void setFavIconDisplay(boolean isVisible);
}
