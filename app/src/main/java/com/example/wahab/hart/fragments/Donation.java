package com.example.wahab.hart.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.wahab.hart.R;

public class Donation extends Fragment {
    TextView donate_more, doller, doller1, doller2, doller3;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitView(view);
        InitListener();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_donation, container, false);


        return view;
    }

    private void InitListener() {
        donate_more.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {

                android.support.v4.app.FragmentTransaction fragmentTransaction;
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame_layout, new DonateMore());
                fragmentTransaction.commitAllowingStateLoss();
            }
        });
        doller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeColor(v);

            }
        });
        doller1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeColor1(v);
            }
        });
        doller2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeColor2(v);
            }
        });
        doller3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeColor3(v);
            }
        });
    }

    private void InitView(View view) {
        donate_more = view.findViewById(R.id.donate_more);

        doller = view.findViewById(R.id.doller);
        doller1 = view.findViewById(R.id.doller1);
        doller2 = view.findViewById(R.id.doller2);
        doller3 = view.findViewById(R.id.doller3);
    }


    public void ChangeColor(View v) {

        setBtnBgColor(doller, R.drawable.border, R.color.bule);
        setBtnBgColor(doller1, R.drawable.donate_amount_border, R.color.doller);
        setBtnBgColor(doller2, R.drawable.donate_amount_border, R.color.doller);
        setBtnBgColor(doller3, R.drawable.donate_amount_border, R.color.doller);
    }

    public void ChangeColor1(View v) {

        setBtnBgColor(doller1, R.drawable.border, R.color.bule);
        setBtnBgColor(doller2, R.drawable.donate_amount_border, R.color.doller);
        setBtnBgColor(doller3, R.drawable.donate_amount_border, R.color.doller);
        setBtnBgColor(doller, R.drawable.donate_amount_border, R.color.doller);
    }

    public void ChangeColor2(View v) {

        setBtnBgColor(doller2, R.drawable.border, R.color.bule);
        setBtnBgColor(doller1, R.drawable.donate_amount_border, R.color.doller);
        setBtnBgColor(doller3, R.drawable.donate_amount_border, R.color.doller);
        setBtnBgColor(doller, R.drawable.donate_amount_border, R.color.doller);
    }

    public void ChangeColor3(View v) {

        setBtnBgColor(doller3, R.drawable.border, R.color.bule);
        setBtnBgColor(doller, R.drawable.donate_amount_border, R.color.doller);
        setBtnBgColor(doller1, R.drawable.donate_amount_border, R.color.doller);
        setBtnBgColor(doller2, R.drawable.donate_amount_border, R.color.doller);

    }

    public void setBtnBgColor(TextView btn, int rId, int rClId) {
        btn.setBackgroundResource(rId);
        btn.setTextColor(btn.getContext().getResources().getColor(rClId));

    }

}
