package com.example.wahab.hart.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.wahab.hart.R;
import com.example.wahab.hart.models.TopicListModel;

import java.util.List;

/**
 * Created by Wahab on 1/23/2018.
 */

public class TopicListAdapter extends RecyclerView.Adapter<TopicListAdapter.MyViewHolder> {

    private List<TopicListModel> List_view;


    public TopicListAdapter(List<TopicListModel> list_view) {
        List_view = list_view;
    }

    @Override
    public TopicListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.topics_list, parent, false);

        return new TopicListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final TopicListAdapter.MyViewHolder holder, int position) {
        TopicListModel frag = List_view.get(position);

        holder.bgColor.setBackgroundColor(Color.parseColor(frag.getBg_color()));
        holder.topic_icon.setImageResource(frag.getTopic_icon());
        holder.topic_name.setText(frag.getTopic_name());
        holder.like.setText(frag.getLike_count());
        holder.dislike.setText(frag.getDislike_count());

        holder.like_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.topic_like.getTag().equals("like")) {
                    holder.topic_like.setTag("unlike");
                    holder.topic_like.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_new));
                    String likeIncrement = holder.like.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value++;
                    holder.like.setText(value + "%");
                } else {
                    holder.topic_like.setTag("like");
                    holder.topic_like.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_icon1));
                    String likeDecrement = holder.like.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeDecrement);
                    value--;
                    holder.like.setText(value + "%");
                }

            }
        });
        holder.dislike_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.topic_dislike.getTag().equals("dislike")) {
                    holder.topic_dislike.setTag("undislike");
                    holder.topic_dislike.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new2));
                    String likeIncrement = holder.dislike.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value--;
                    holder.dislike.setText(value + "%");
                } else {
                    holder.topic_dislike.setTag("dislike");
                    holder.topic_dislike.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new));
                    String likeIncrement = holder.dislike.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value++;
                    holder.dislike.setText(value + "%");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return List_view.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView topic_icon, topic_like, topic_dislike;
        public TextView topic_name;
        public TextView like;
        public TextView dislike;
        public LinearLayout bgColor, like_view, dislike_view;


        public MyViewHolder(View view) {
            super(view);
            bgColor = view.findViewById(R.id.topic_bg);
            topic_icon = view.findViewById(R.id.topicIcon);
            topic_name = view.findViewById(R.id.topicName);
            like = view.findViewById(R.id.like);
            dislike = view.findViewById(R.id.dislike);

            //like dislike image
            topic_like = view.findViewById(R.id.topic_like);
            topic_dislike = view.findViewById(R.id.topic_dislike);

            //like dislike view
            like_view = view.findViewById(R.id.like_view);
            dislike_view = view.findViewById(R.id.dislike_view);
        }
    }

}
