package com.example.wahab.hart.network.model;

public class Response {
    public String response_data;
    public String status;
    public String error;
    public String returnType;
    public String token;
    public String img_data;
    public String message;
    public String Id;
}
