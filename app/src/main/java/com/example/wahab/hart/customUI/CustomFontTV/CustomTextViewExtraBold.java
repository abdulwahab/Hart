package com.example.wahab.hart.customUI.CustomFontTV;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.example.wahab.hart.customUI.CustomTV;
import com.example.wahab.hart.customUI.FontCache;

/**
 * Created by Wahab on 1/31/2018.
 */

public class CustomTextViewExtraBold extends CustomTV {
    public CustomTextViewExtraBold(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public CustomTextViewExtraBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public CustomTextViewExtraBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("WorkSans-ExtraBold.ttf", context);
        setTypeface(customFont, Typeface.NORMAL);
//        setLinksClickable(true);
//        setAutoLinkMask(Linkify.WEB_URLS);
    }

}
