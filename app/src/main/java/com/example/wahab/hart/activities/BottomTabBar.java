package com.example.wahab.hart.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.wahab.hart.R;
import com.example.wahab.hart.fragments.Donation;
import com.example.wahab.hart.fragments.FeedPost;
import com.example.wahab.hart.fragments.SinglePost;
import com.example.wahab.hart.fragments.Topics;

import static com.example.wahab.hart.staticfunction.UIModification.ChangeStatusBarColor;

public class BottomTabBar extends AppCompatActivity implements View.OnClickListener {
    LinearLayout home, topic, volunteer, donation;
    private ImageView home_ic, topic_ic, volunteer_ic, donation_ic;
    TextView home_text, topic_text, volunteer_text, donation_text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_tab_bar);
        ChangeStatusBarColor(BottomTabBar.this, "#981b1e");
        InitView();
        InitListener();

    }

    private void InitListener() {
        TabHome();
        ChangeFragment(new SinglePost());

        //Bottom tab bar button
        home.setOnClickListener(this);
        topic.setOnClickListener(this);
        volunteer.setOnClickListener(this);
        donation.setOnClickListener(this);
    }

    private void InitView() {
        //Image ids
        home_ic = findViewById(R.id.home_icon);
        topic_ic = findViewById(R.id.topic_icon);
        volunteer_ic = findViewById(R.id.volunteer_icon);
        donation_ic = findViewById(R.id.donation_icon);

        //TextView ids
        home_text = findViewById(R.id.home_text);
        topic_text = findViewById(R.id.topic_text);
        volunteer_text = findViewById(R.id.volunteer_text);
        donation_text = findViewById(R.id.donation_text);

        //Linear layout
        home = findViewById(R.id.home);
        topic = findViewById(R.id.topic);
        volunteer = findViewById(R.id.volunteer);
        donation = findViewById(R.id.donation);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home:
                TabHome();
                ChangeFragment(new SinglePost());
                break;
            case R.id.topic:
                TabTopic();
                ChangeFragment(new Topics());
                break;
            case R.id.volunteer:
                TabVolunteers();
                ChangeFragment(new FeedPost());
                break;
            case R.id.donation:
                TabDonation();
                ChangeFragment(new Donation());
                break;
        }
    }

    public void TabHome() {
        home_ic.setImageDrawable(getResources().getDrawable(R.drawable.home_new));
        topic_ic.setImageDrawable(getResources().getDrawable(R.drawable.topics));
        volunteer_ic.setImageDrawable(getResources().getDrawable(R.drawable.volunteers));
        donation_ic.setImageDrawable(getResources().getDrawable(R.drawable.donation));

        home_text.setTextColor(getResources().getColor(R.color.red));
        topic_text.setTextColor(getResources().getColor(R.color.textClr));
        volunteer_text.setTextColor(getResources().getColor(R.color.textClr));
        donation_text.setTextColor(getResources().getColor(R.color.textClr));
    }

    public void TabTopic() {
        home_ic.setImageDrawable(getResources().getDrawable(R.drawable.home1));
        topic_ic.setImageDrawable(getResources().getDrawable(R.drawable.topic_new));
        volunteer_ic.setImageDrawable(getResources().getDrawable(R.drawable.volunteers));
        donation_ic.setImageDrawable(getResources().getDrawable(R.drawable.donation));

        home_text.setTextColor(getResources().getColor(R.color.textClr));
        topic_text.setTextColor(getResources().getColor(R.color.red));
        volunteer_text.setTextColor(getResources().getColor(R.color.textClr));
        donation_text.setTextColor(getResources().getColor(R.color.textClr));
    }

    public void TabVolunteers() {
        home_ic.setImageDrawable(getResources().getDrawable(R.drawable.home1));
        topic_ic.setImageDrawable(getResources().getDrawable(R.drawable.topics));
        volunteer_ic.setImageDrawable(getResources().getDrawable(R.drawable.volunteers_new));
        donation_ic.setImageDrawable(getResources().getDrawable(R.drawable.donation));

        home_text.setTextColor(getResources().getColor(R.color.textClr));
        topic_text.setTextColor(getResources().getColor(R.color.textClr));
        volunteer_text.setTextColor(getResources().getColor(R.color.red));
        donation_text.setTextColor(getResources().getColor(R.color.textClr));
    }

    public void TabDonation() {
        home_ic.setImageDrawable(getResources().getDrawable(R.drawable.home1));
        topic_ic.setImageDrawable(getResources().getDrawable(R.drawable.topics));
        volunteer_ic.setImageDrawable(getResources().getDrawable(R.drawable.volunteers));
        donation_ic.setImageDrawable(getResources().getDrawable(R.drawable.donation_new));

        home_text.setTextColor(getResources().getColor(R.color.textClr));
        topic_text.setTextColor(getResources().getColor(R.color.textClr));
        volunteer_text.setTextColor(getResources().getColor(R.color.textClr));
        donation_text.setTextColor(getResources().getColor(R.color.red));
    }

    public void ChangeFragment(Fragment fragmentName) {
        FragmentTransaction fragmentTransaction;
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fragmentTransaction.replace(R.id.frame_layout, fragmentName);
        fragmentTransaction.commitAllowingStateLoss();

    }


}
