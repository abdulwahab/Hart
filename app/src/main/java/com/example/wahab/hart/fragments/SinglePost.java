package com.example.wahab.hart.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.wahab.hart.R;
import com.example.wahab.hart.adapters.CommentListAdapter;
import com.example.wahab.hart.adapters.RelatedPostListAdapter;
import com.example.wahab.hart.models.CommentListModel;
import com.example.wahab.hart.models.RelatPostModel;

import java.util.ArrayList;
import java.util.List;


public class SinglePost extends Fragment {
    private List<RelatPostModel> list = new ArrayList<>();
    private List<CommentListModel> list1 = new ArrayList<>();
    private RecyclerView recyclerView, recyclerView1;
    private RelatedPostListAdapter postAdapter;
    private CommentListAdapter commentAdapter;
    RelativeLayout readMore;
    private LinearLayout agree, disagree, like_view_single;
    private ImageView agree_icon, disagree_icon, like_sp;
    TextView like_value_sp;


    public SinglePost() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitView(view);
        InitListener();
        InitRecyclerView();

        prepareFragmentData();
        prepareFragmentData_comment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_single_post, container, false);


        return view;
    }

    public void InitView(View view) {
        //Agree Disagree box
        agree = view.findViewById(R.id.agree);
        disagree = view.findViewById(R.id.disagree);

        agree_icon = view.findViewById(R.id.agree_icon);
        disagree_icon = view.findViewById(R.id.disagree_icon);

        //Post Recycler view
        recyclerView = view.findViewById(R.id.recyclerView_related_post);
        recyclerView.setNestedScrollingEnabled(false);
        postAdapter = new RelatedPostListAdapter(list);

        //Comment Recycler view
        recyclerView1 = view.findViewById(R.id.recyclerView_comment);
        recyclerView1.setNestedScrollingEnabled(false);
        commentAdapter = new CommentListAdapter(list1);


        readMore = view.findViewById(R.id.readMore);

        //Top single post like
        like_view_single = view.findViewById(R.id.like_view_single);
        like_sp = view.findViewById(R.id.like_sp);
        like_value_sp = view.findViewById(R.id.like_value_sp);

    }

    private void InitRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(postAdapter);


        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getContext());
        recyclerView1.setLayoutManager(mLayoutManager1);
        recyclerView1.setItemAnimator(new DefaultItemAnimator());
        recyclerView1.setAdapter(commentAdapter);

    }


    private void InitListener() {
        readMore.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {

                android.support.v4.app.FragmentTransaction fragmentTransaction;
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame_layout, new TopicPost());
                fragmentTransaction.commitAllowingStateLoss();
            }
        });

        like_view_single.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (like_sp.getTag().equals("like")) {
                    like_sp.setTag("unlike");
                    like_sp.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_icon1));
                    String likeIncrement = like_value_sp.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value--;
                    like_value_sp.setText(value + "%");
                } else {
                    like_sp.setTag("like");
                    like_sp.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_new));
                    String likeIncrement = like_value_sp.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value++;
                    like_value_sp.setText(value + "%");
                }

            }
        });

        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (agree_icon.getTag().equals("like")) {
                    agree_icon.setTag("unlike");
                    agree_icon.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_icon1));
                } else {
                    agree_icon.setTag("like");
                    agree_icon.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_new));
                }

            }
        });
        disagree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (disagree_icon.getTag().equals("like")) {
                    disagree_icon.setTag("unlike");
                    disagree_icon.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new2));
                } else {
                    disagree_icon.setTag("like");
                    disagree_icon.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new));
                }

            }
        });

    }

    private void prepareFragmentData() {
        list.clear();

        RelatPostModel list_data = new RelatPostModel(R.drawable.teaching, "college education neends to be more affordable", "January 3,2018", "15%", "65%");
        list.add(list_data);

        list_data = new RelatPostModel(R.drawable.college1, "college education neends to be more affordable", "January 3,2018", "15%", "65%");
        list.add(list_data);

        list_data = new RelatPostModel(R.drawable.teaching2, "college education neends to be more affordable", "January 3,2018", "15%", "65%");
        list.add(list_data);
//
//        list_data = new RelatPostModel(R.drawable.video1,"college education neends to be more affordable","January 3,2018","15%","65%");
//        list.add(list_data);
//
//        list_data = new RelatPostModel(R.drawable.video1,"college education neends to be more affordable","January 3,2018","15%","65%");
//        list.add(list_data);

        postAdapter.notifyDataSetChanged();
    }

    private void prepareFragmentData_comment() {
        list1.clear();

        CommentListModel list_data = new CommentListModel("Igor Savelev", "one hour ago", "college education neends to be more affordable college education neends to be more affordable", "56");
        list1.add(list_data);

//        list_data = new CommentListModel("Igor Savelev","one hour ago", "The quick brown fox jumped over the lazy dog","56" );
//        list1.add(list_data);
//
//        list_data = new CommentListModel("Igor Savelev","one hour ago", "The quick brown fox jumped over the lazy dog","56" );
//        list1.add(list_data);

        commentAdapter.notifyDataSetChanged();
    }
}
