package com.example.wahab.hart.customUI.CustomFontET;

import android.content.Context;
import android.graphics.Typeface;
import android.text.util.Linkify;
import android.util.AttributeSet;

import com.example.wahab.hart.customUI.CustomET;
import com.example.wahab.hart.customUI.FontCache;

/**
 * Created by Wahab on 1/31/2018.
 */

public class CustomEditTextViewOpen extends CustomET {
    public CustomEditTextViewOpen(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public CustomEditTextViewOpen(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public CustomEditTextViewOpen(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("OpenSans-Bold.ttf", context);
        setTypeface(customFont, Typeface.NORMAL);
        setLinksClickable(true);
        setAutoLinkMask(Linkify.WEB_URLS);
    }

}
