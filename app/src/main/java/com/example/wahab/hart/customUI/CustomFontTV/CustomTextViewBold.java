package com.example.wahab.hart.customUI.CustomFontTV;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.example.wahab.hart.customUI.CustomTV;
import com.example.wahab.hart.customUI.FontCache;

/**
 * Created by Wahab on 1/31/2018.
 */

public class CustomTextViewBold extends CustomTV {
    public CustomTextViewBold(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public CustomTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public CustomTextViewBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("WorkSans-Bold.ttf", context);
        setTypeface(customFont, Typeface.NORMAL);
//        setLinksClickable(true);
//        setAutoLinkMask(Linkify.WEB_URLS);
    }

}
