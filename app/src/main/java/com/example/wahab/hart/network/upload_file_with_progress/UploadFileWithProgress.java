package com.example.wahab.hart.network.upload_file_with_progress;


import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

//import com.example.codingpixel.i_launched.customui.ProgressDialog;
//import com.example.codingpixel.i_launched.data_structure.APIActions;
//import com.example.codingpixel.i_launched.interfaces.FileUploadProgressListner;
//import com.example.codingpixel.i_launched.network.model.APIResponseListner;

import com.example.wahab.hart.customUI.ProgressDialog;
import com.example.wahab.hart.data_structure.APIActions;
import com.example.wahab.hart.interfaces.FileUploadProgressListner;
import com.example.wahab.hart.network.model.APIResponseListner;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;


public class UploadFileWithProgress extends AsyncTask<String, Integer, String> {
    private String Api_url;
    private String File_path;
    private String File_Type;
    private long totalSize = 0;
    private final ProgressDialog pd;
    private FileUploadProgressListner progressListener;
    private APIResponseListner apiResponseListner;
    private JSONArray parameters;
    private JSONArray parameters_key;
    private Context context;
    private Boolean isLoading;
    private APIActions.ApiActions apiActions;

    public UploadFileWithProgress(Context context, Boolean isLoading, String api_url, String file_path, String file_type, JSONArray parameters, JSONArray parameters_key, FileUploadProgressListner progressListener, APIResponseListner apiResponseListner, APIActions.ApiActions apiActions) {
        this.Api_url = api_url;
        this.File_path = file_path;
        this.File_Type = file_type;
        this.progressListener = progressListener;
        this.apiResponseListner = apiResponseListner;
        this.parameters = parameters;
        this.parameters_key = parameters_key;
        pd = ProgressDialog.newInstance();
        this.apiActions = apiActions;
        this.context = context;
        this.isLoading = isLoading;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (isLoading) {
            pd.show(((FragmentActivity) context).getSupportFragmentManager(), "pd");
        }
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        if (progressListener != null) {
            progressListener.UploadedProgress(progress[0]);
        }
    }

    @Override
    protected String doInBackground(String... params) {
        return uploadFile(File_path);
    }


    private String uploadFile(String path) {
        String responseString = null;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(Api_url);
//            postRequest.setHeader("session_token", user.getSession_key());
            postRequest.setHeader("app_key", "4m9Nv1nbyLoaZAMyAhQri9BUXBxlD3yQxbAiHsn2hwQ=");
            File file = new File(path);
            AndroidMultiPartEntity reqEntity = new AndroidMultiPartEntity(
                    new AndroidMultiPartEntity.ProgressListener() {
                        @Override
                        public void transferred(long num) {
                            publishProgress(((int) ((num / (float) totalSize) * 100)));
                        }
                    });
            if (File_Type.equalsIgnoreCase("image")) {
                ContentType contentType = ContentType.create("image/jpeg");
                FileBody cbFile = new FileBody(file, contentType.getMimeType(), file.getName());
                reqEntity.addPart("image", cbFile);
            } else if (File_Type.equalsIgnoreCase("video")) {
                ContentType contentType = ContentType.create("video/mp4");
                FileBody cbFile = new FileBody(file, contentType.getMimeType(), file.getName());
                reqEntity.addPart("video", cbFile);
            }
            for (int x = 0; x < parameters.length(); x++) {
                reqEntity.addPart(parameters_key.getString(x), new StringBody(parameters.getString(x)));
            }
            totalSize = reqEntity.getContentLength();
            postRequest.setEntity(reqEntity);
            HttpResponse response = httpClient.execute(postRequest);
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    response.getEntity().getContent(), "UTF-8"));
            int code = response.getStatusLine().getStatusCode();
            String sResponse;
            StringBuilder s = new StringBuilder();
            while ((sResponse = reader.readLine()) != null) {
                s = s.append(sResponse);
            }
            reader.close();
            System.out.println("Response: " + s);
            System.out.println("CODE: " + "" + code);
            return "" + s;

        } catch (Exception e) {
            // handle exception here
            Log.e(e.getClass().getName(), e.getMessage());
            return e.getMessage();
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        pd.dismiss();
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                apiResponseListner.onRequestSuccess(result, apiActions);
            } else {
                apiResponseListner.onRequestError(result, apiActions);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}