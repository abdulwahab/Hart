package com.example.wahab.hart.adapters;

import android.graphics.Color;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.wahab.hart.R;
import com.example.wahab.hart.models.CombineListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wahab on 1/24/2018.
 */

public class CombineListAdapter extends RecyclerView.Adapter<CombineListAdapter.MyViewHolder> {

    private List<CombineListModel> List_view = new ArrayList<>();


    public CombineListAdapter(List<CombineListModel> list_view) {
        List_view = list_view;
    }

    @Override
    public CombineListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.combine_item_view, parent, false);

        return new CombineListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CombineListAdapter.MyViewHolder holder, int position) {
        CombineListModel frag = List_view.get(position);

        if (position == (List_view.size() - 1)) {
            holder.btn_load_more.setVisibility(View.VISIBLE);
        } else {
            holder.btn_load_more.setVisibility(View.GONE);
        }

        holder.top_like_fp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.like_upeer_fp.getTag().equals("like")) {
                    holder.like_upeer_fp.setTag("unlike");
                    holder.like_upeer_fp.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_icon1));
                    String likeIncrement = holder.like_value_fp.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value--;
                    holder.like_value_fp.setText(value + "%");
                } else {
                    holder.like_upeer_fp.setTag("like");
                    holder.like_upeer_fp.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_new));
                    String likeIncrement = holder.like_value_fp.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value++;
                    holder.like_value_fp.setText(value + "%");
                }
            }
        });

        if (frag.isCheck()) {
            holder.main_bg.setBackgroundColor(holder.main_bg.getContext().getResources().getColor(R.color.bg_feedPost));
            if (frag.isFirstPost()) {
                holder.title.setVisibility(View.GONE);
                holder.top_like.setVisibility(View.VISIBLE);
            } else {
                holder.title.setVisibility(View.VISIBLE);
                holder.title.setText("Top \n Posts");
                holder.title.setTextColor(Color.parseColor("#05324f"));
                holder.top_like.setVisibility(View.VISIBLE);
            }
            holder.recyclerView_combine.setVisibility(View.VISIBLE);
            holder.social_post.setVisibility(View.GONE);
            holder.topic_post.setVisibility(View.GONE);

            GridAdapterView abc = new GridAdapterView(frag.getObject_top());
            holder.recyclerView_combine.setLayoutManager(new GridLayoutManager(holder.recyclerView_combine.getContext(), 2));
            holder.recyclerView_combine.setItemAnimator(new DefaultItemAnimator());
            holder.recyclerView_combine.setAdapter(abc);

        } else if (frag.isCheckOne()) {
            if (frag.getHeadTitle().equalsIgnoreCase("Recent Articles")) {
                holder.main_bg.setBackgroundColor(holder.main_bg.getContext().getResources().getColor(R.color.white));
            } else {
                holder.main_bg.setBackgroundColor(holder.main_bg.getContext().getResources().getColor(R.color.bg_feedPost));
            }
            if (frag.isFirstPost()) {
                holder.title.setVisibility(View.VISIBLE);
                holder.title.setText(frag.getHeadTitle());
                holder.title.setTextColor(Color.parseColor(frag.getHeadColor()));
                holder.top_like.setVisibility(View.GONE);

            } else {
                holder.title.setVisibility(View.GONE);
                holder.top_like.setVisibility(View.GONE);
            }
            if (frag.isMore_option()) {
                holder.read_more_topic_common.setVisibility(View.VISIBLE);
            } else {
                holder.read_more_topic_common.setVisibility(View.GONE);
            }

            holder.recyclerView_combine.setVisibility(View.GONE);
            holder.social_post.setVisibility(View.VISIBLE);
            holder.topic_post.setVisibility(View.GONE);

            holder.post_img.setBackgroundResource(frag.getObject_post().getPost_Image());
            holder.post_heading.setText(frag.getObject_post().getPost_heading());
            holder.post_date.setText(frag.getObject_post().getPost_date());
            holder.like_value.setText(frag.getObject_post().getPost_like());
            holder.dislike_value.setText(frag.getObject_post().getPost_dislike());

            holder.like_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.like_icon.getTag().equals("like")) {
                        holder.like_icon.setTag("unlike");
                        holder.like_icon.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_new));
                        String likeIncrement = holder.like_value.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value++;
                        holder.like_value.setText(value + "%");
                    } else {
                        holder.like_icon.setTag("like");
                        holder.like_icon.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_icon1));
                        String likeIncrement = holder.like_value.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value--;
                        holder.like_value.setText(value + "%");
                    }

                }
            });
            holder.dislike_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.dislike_icon.getTag().equals("dislike")) {
                        holder.dislike_icon.setTag("undislike");
                        holder.dislike_icon.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new2));
                        String likeIncrement = holder.dislike_value.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value--;
                        holder.dislike_value.setText(value + "%");
                    } else {
                        holder.dislike_icon.setTag("dislike");
                        holder.dislike_icon.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new));
                        String likeIncrement = holder.dislike_value.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value++;
                        holder.dislike_value.setText(value + "%");
                    }
                }
            });

        } else if (frag.isCheckTow()) {
            holder.main_bg.setBackgroundColor(holder.main_bg.getContext().getResources().getColor(android.R.color.white));
            if (frag.isFirstMore()) {
                holder.title.setVisibility(View.VISIBLE);
                holder.title.setText("COMMUNITY \nCONTENT");
                holder.title.setTextColor(Color.parseColor("#05324f"));
            } else {
                holder.title.setVisibility(View.GONE);
            }
            if (frag.isLast()) {
                holder.read_more_topic.setVisibility(View.VISIBLE);
            } else {
                holder.read_more_topic.setVisibility(View.GONE);
            }

            holder.recyclerView_combine.setVisibility(View.GONE);
            holder.social_post.setVisibility(View.GONE);
            holder.topic_post.setVisibility(View.VISIBLE);

            holder.banner.setBackgroundResource(frag.getObject_more().getTopic_icon());
            holder.heading.setText(frag.getObject_more().getTopic_name());
            holder.postTime.setText(frag.getObject_more().getTopic_date());
            holder.post_like.setText(frag.getObject_more().getLike_count());
            holder.post_dislike.setText(frag.getObject_more().getDislike_count());

            holder.like_view_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.like_more.getTag().equals("like")) {
                        holder.like_more.setTag("unlike");
                        holder.like_more.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_new));
                        String likeIncrement = holder.post_like.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value++;
                        holder.post_like.setText(value + "%");
                    } else {
                        holder.like_more.setTag("like");
                        holder.like_more.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_icon1));
                        String likeIncrement = holder.post_like.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value--;
                        holder.post_like.setText(value + "%");
                    }

                }
            });
            holder.dislike_view_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.dislike_more.getTag().equals("dislike")) {
                        holder.dislike_more.setTag("undislike");
                        holder.dislike_more.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new2));
                        String likeIncrement = holder.post_dislike.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value--;
                        holder.post_dislike.setText(value + "%");
                    } else {
                        holder.dislike_more.setTag("dislike");
                        holder.dislike_more.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new));
                        String likeIncrement = holder.post_dislike.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value++;
                        holder.post_dislike.setText(value + "%");
                    }
                }
            });

        }


    }

    @Override
    public int getItemCount() {
        return List_view.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //Top like button feed post
        TextView like_value_fp;
        LinearLayout top_like_fp;
        ImageView like_upeer_fp;

        public View top_post, topic_post, social_post;
        public RecyclerView recyclerView_combine;
        public LinearLayout main_bg;

        //Top heading bar / common
        public TextView title, btn_load_more;
        public LinearLayout top_like;

        //Top post
        public LinearLayout cover_img;
        public TextView cover_title, cover_date, like_gv, dislike_gv;

        //Recent ,society ,foreign ,domestic ,education
        public TextView post_heading, post_date, like_value, dislike_value;
        public RelativeLayout post_img, read_more_topic_common;
        public ImageView like_icon, dislike_icon;
        public LinearLayout like_view;
        public LinearLayout dislike_view;

        //More Topics
        public LinearLayout banner;
        public TextView heading, postTime, post_like, post_dislike;
        public RelativeLayout read_more_topic;
        public ImageView like_more, dislike_more;
        LinearLayout like_view_more, dislike_view_more;


        public MyViewHolder(View view) {
            super(view);
            //Top like button feed post
            top_like_fp = view.findViewById(R.id.top_like_fp);
            like_upeer_fp = view.findViewById(R.id.like_upeer_fp);
            like_value_fp = view.findViewById(R.id.like_value_fp);

            recyclerView_combine = view.findViewById(R.id.recyclerView_combine);
            main_bg = view.findViewById(R.id.main_bg);

            btn_load_more = view.findViewById(R.id.btn_load_more);
//            top_post = view.findViewById(R.id.top_post);
            topic_post = view.findViewById(R.id.topic_post);
            social_post = view.findViewById(R.id.social_post);
            title = view.findViewById(R.id.title);
            top_like = view.findViewById(R.id.top_like_fp);

            //Top post
            cover_img = view.findViewById(R.id.cover_img);
            cover_title = view.findViewById(R.id.cover_title);
            cover_date = view.findViewById(R.id.cover_date);
            like_gv = view.findViewById(R.id.like_gv);
            dislike_gv = view.findViewById(R.id.dislike_gv);

            //Recent ,society ,foreign ,domestic ,education
            post_heading = view.findViewById(R.id.post_heading);
            post_date = view.findViewById(R.id.post_date);
            like_value = view.findViewById(R.id.like_value);
            dislike_value = view.findViewById(R.id.dislike_value);
            post_img = view.findViewById(R.id.post_img);
            read_more_topic_common = view.findViewById(R.id.read_more_topic_common);

            //Like Dislike icon
            like_icon = view.findViewById(R.id.like_icon);
            dislike_icon = view.findViewById(R.id.dislike_icon);
            like_view = view.findViewById(R.id.like_view);
            dislike_view = view.findViewById(R.id.dislike_view);


            //More Topics
            banner = view.findViewById(R.id.banner);
            heading = view.findViewById(R.id.heading);
            postTime = view.findViewById(R.id.postTime);
            post_like = view.findViewById(R.id.post_like);
            post_dislike = view.findViewById(R.id.post_dislike);
            read_more_topic = view.findViewById(R.id.read_more_topic);

            //like-dislike
            like_view_more = view.findViewById(R.id.like_view_topic);
            dislike_view_more = view.findViewById(R.id.dislike_view_topic);
            like_more = view.findViewById(R.id.like);
            dislike_more = view.findViewById(R.id.dislike);

        }
    }
}
