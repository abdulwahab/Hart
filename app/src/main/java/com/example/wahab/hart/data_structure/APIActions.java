package com.example.wahab.hart.data_structure;

public class APIActions {
    public static enum ApiActions {
        testAPI, check_emai, get_defaults, register, upload_profile_img, login, get_Experties, post_Experties,
        ask_question, get_activities, get_my_question, get_question_answers, add_answer, add_media, add_question_like_dislike,
        answer_like, add_answer_flag, get_user_profile, update_avator, update_profile_info, update_user_email,
        follow_unfollow_user, update_experties, get_chats, get_detail_chat, send_message, add_message_like,
        send_message_meida, send_message_after_media, send_msg_video, search_chat_user, get_groups, get_groups_with_pagination, get_messgaes,
        add_group, get_tags, join_group, delete_Chat, get_users, invite_members_for_group, remove_user_group, group_refresh_call,
        accept_reject_invitaion, remove_group, leave_group, get_user_groups, get_user_profile_strains, create_journal,
        get_journals, get_journals_events, follow_journal, favourtire_journal, add_group_settings, hb_gallery, group_settings,
        create_journal_event, delete_journal_event, edit_group, get_strains, save_favorit_strain, strain_flaged, get_strain_detail,
        add_strain_review, upload_strain_image, flag_strain_review , get_budz_map,delete_my_save,get_my_strains,filter_my_save,get_notifications,get_shout_outs
        ,add_shout_out,delete_answer,global_search,get_my_journals_calander,get_journal_settings,add_subscription,get_languages,get_budz_profile,
        delete_question,get_strains_matched,save_survey_answer, save_user_strain,get_user_strain,add_listing,save_favorit_bud,
        get_invite, get_notification, get_remove_tag, get_key, get_followers,get_followings,get_journal_settings_update, add_budz_review, add_budz_review_flag, get_budz_profile_can, get_budz_profile_event, get_budz_profile_overall, get_strains_detail, get_suggestion, add_flag_q, get_journal
    }
}
