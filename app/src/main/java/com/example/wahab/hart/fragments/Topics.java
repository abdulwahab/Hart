package com.example.wahab.hart.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.wahab.hart.R;
import com.example.wahab.hart.adapters.TopicListAdapter;
import com.example.wahab.hart.models.TopicListModel;

import java.util.ArrayList;
import java.util.List;


public class Topics extends Fragment {
    private List<TopicListModel> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private TopicListAdapter topicAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_topics, container, false);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitView(view);
        InitRecyclerView();

        prepareFragmentData();
    }

    private void InitRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(topicAdapter);
    }

    private void InitView(View view) {
        recyclerView = view.findViewById(R.id.recyclerView_topics);
        topicAdapter = new TopicListAdapter(list);
    }

    private void prepareFragmentData() {
        list.clear();

        TopicListModel list_data = new TopicListModel(R.drawable.social, "social", "Jan 3, 2018", "32%", "67%", "#d500f9");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.economy, "economic", "Jan 3, 2018", "32%", "67%", "#ffce00");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.national_security, "national security", "Jan 3, 2018", "32%", "67%", "#afb42b");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.environmental, "environmental", "Jan 3, 2018", "32%", "67%", "#228722");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.family_values, "family values", "Jan 3, 2018", "32%", "67%", "#f70000");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.freedom, "freedom", "Jan 3, 2018", "32%", "67%", "#ecf2f7");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.healthcare, "healthcare", "Jan 3, 2018", "32%", "67%", "#2e43aa");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.women, "women", "Jan 3, 2018", "32%", "67%", "#ed5690");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.foreign_policy, "foreign policy", "Jan 3, 2018", "32%", "67%", "#774d40");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.children, "children", "Jan 3, 2018", "32%", "67%", "#aad9f9");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.home1, "domestic policy", "Jan 3, 2018", "32%", "67%", "#ededed");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.immigration, "immigration", "Jan 3, 2018", "32%", "67%", "#ffec5f");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.science, "science & technology", "Jan 3, 2018", "32%", "67%", "#a0a0a0");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.agriculture, "agriculture", "Jan 3, 2018", "32%", "67%", "#76d376");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.religion, "religion", "Jan 3, 2018", "32%", "67%", "#ffffff");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.education, "education", "Jan 3, 2018", "32%", "67%", "#e54174");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.transporation, "transporation", "Jan 3, 2018", "32%", "67%", "#ffd092");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.criminal, "criminal", "Jan 3, 2018", "32%", "67%", "#1b7bed");
        list.add(list_data);

        list_data = new TopicListModel(R.drawable.electoral, "electoral", "Jan 3, 2018", "32%", "67%", "#ffe708");
        list.add(list_data);


        topicAdapter.notifyDataSetChanged();
    }


}
