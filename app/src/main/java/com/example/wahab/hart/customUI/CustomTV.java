package com.example.wahab.hart.customUI;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Wahab on 1/31/2018.
 */

@SuppressLint("AppCompatCustomView")
public class CustomTV extends TextView {
    public CustomTV(Context context) {
        super(context);
    }

    public CustomTV(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTV(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
