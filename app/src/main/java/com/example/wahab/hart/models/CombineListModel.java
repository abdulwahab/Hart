package com.example.wahab.hart.models;

import java.util.List;

/**
 * Created by Wahab on 1/25/2018.
 */

public class CombineListModel {
    private String HeadTitle, HeadColor;
    List<TopPostListModel> object_top;
    boolean check, isFirstTop, isFirstTopLike, isLastPost;
    RelatPostModel object_post;
    boolean checkOne, isFirstPost, more_option;
    TopicListModel object_more;
    boolean checkTow, isFirstMore, isLast;

    public CombineListModel(List<TopPostListModel> object_top, boolean check, boolean isFirstTop, boolean isFirstTopLike, boolean isLastPost) {
        this.object_top = object_top;
        this.check = check;
        this.isFirstTop = isFirstTop;
        this.isFirstTopLike = isFirstTopLike;
        this.isLastPost = isLastPost;
    }

    public CombineListModel(RelatPostModel object_post, boolean checkOne, boolean isFirstPost, boolean more_option) {
        this.object_post = object_post;
        this.checkOne = checkOne;
        this.isFirstPost = isFirstPost;
        this.more_option = more_option;
    }

    public CombineListModel(RelatPostModel object_post, boolean checkOne, boolean isFirstPost, boolean more_option, String HeadTitle, String HeadColor) {
        this.object_post = object_post;
        this.checkOne = checkOne;
        this.isFirstPost = isFirstPost;
        this.more_option = more_option;
        this.HeadTitle = HeadTitle;
        this.HeadColor = HeadColor;
    }

    public CombineListModel(TopicListModel object_more, boolean checkTow, boolean isFirstMore, boolean isLast) {
        this.object_more = object_more;
        this.checkTow = checkTow;
        this.isFirstMore = isFirstMore;
        this.isLast = isLast;
    }

    public boolean isLast() {
        return isLast;
    }

    public void setLast(boolean last) {
        isLast = last;
    }

    public String getHeadTitle() {
        return HeadTitle;
    }

    public String getHeadColor() {
        return HeadColor;
    }

    public void setHeadColor(String headColor) {
        HeadColor = headColor;
    }

    public void setHeadTitle(String headTitle) {
        HeadTitle = headTitle;
    }

    public boolean isMore_option() {
        return more_option;
    }

    public void setMore_option(boolean more_option) {
        this.more_option = more_option;
    }

    public boolean isLastPost() {
        return isLastPost;
    }

    public void setLastPost(boolean lastPost) {
        isLastPost = lastPost;
    }

    public List<TopPostListModel> getObject_top() {
        return object_top;
    }

    public void setObject_top(List<TopPostListModel> object_top) {
        this.object_top = object_top;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public boolean isFirstTop() {
        return isFirstTop;
    }

    public void setFirstTop(boolean firstTop) {
        isFirstTop = firstTop;
    }

    public boolean isFirstTopLike() {
        return isFirstTopLike;
    }

    public void setFirstTopLike(boolean firstTopLike) {
        isFirstTopLike = firstTopLike;
    }

    public RelatPostModel getObject_post() {
        return object_post;
    }

    public void setObject_post(RelatPostModel object_post) {
        this.object_post = object_post;
    }

    public boolean isCheckOne() {
        return checkOne;
    }

    public void setCheckOne(boolean checkOne) {
        this.checkOne = checkOne;
    }

    public boolean isFirstPost() {
        return isFirstPost;
    }

    public void setFirstPost(boolean firstPost) {
        isFirstPost = firstPost;
    }

    public TopicListModel getObject_more() {
        return object_more;
    }

    public void setObject_more(TopicListModel object_more) {
        this.object_more = object_more;
    }

    public boolean isCheckTow() {
        return checkTow;
    }

    public void setCheckTow(boolean checkTow) {
        this.checkTow = checkTow;
    }

    public boolean isFirstMore() {
        return isFirstMore;
    }

    public void setFirstMore(boolean firstMore) {
        isFirstMore = firstMore;
    }
}
