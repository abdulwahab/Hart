package com.example.wahab.hart.interfaces;

/**
 * Created by jawadali on 12/12/17.
 */

public interface getKeywordDialogItemClickListner {
    public void ShowQuestionsForKeyword(String keyword);

    public void ShowAnswersForKeyword(String keyword);

    public void ShowGroupsForKeyword(String keyword);

    public void ShowJournalsForKeyword(String keyword);

    public void ShowStrainForKeyword(String keyword);

    public void ShowBudzMapForKeyword(String keyword);
}
