package com.example.wahab.hart.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.wahab.hart.R;
import com.example.wahab.hart.adapters.CombineListAdapter;
import com.example.wahab.hart.models.CombineListModel;
import com.example.wahab.hart.models.RelatPostModel;
import com.example.wahab.hart.models.TopPostListModel;
import com.example.wahab.hart.models.TopicListModel;

import java.util.ArrayList;
import java.util.List;


public class FeedPost extends Fragment {
    private List<CombineListModel> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private CombineListAdapter combineAdapter;
    TextView btn_load_more, like_value_fp;
    LinearLayout top_like_fp;
    ImageView like_upeer_fp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed_post, container, false);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitView(view);
        InitRecyclerView();
        listPostSocialData();
        InitListener();
    }

    private void InitRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(combineAdapter);
    }

    private void InitView(View view) {
        btn_load_more = view.findViewById(R.id.btn_load_more);
        top_like_fp = view.findViewById(R.id.top_like_fp);
        like_upeer_fp = view.findViewById(R.id.like_upeer_fp);
        like_value_fp = view.findViewById(R.id.like_value_fp);
        recyclerView = view.findViewById(R.id.recyclerView_combine);
        combineAdapter = new CombineListAdapter(list);
    }

    public void InitListener() {

    }

    private void listPostSocialData() {
        list.clear();
        List<TopPostListModel> temp = new ArrayList<>();
        TopPostListModel list_dataa = new TopPostListModel(R.drawable.personal_health, "Biking Increases \n Urban and Personal \n Health", "January 3,2018", "15%", "65%");
        temp.add(list_dataa);
        list_dataa = new TopPostListModel(R.drawable.teaching, "Biking Increases \n Urban and Personal \n Health", "January 3,2018", "15%", "65%");
        temp.add(list_dataa);
        list_dataa = new TopPostListModel(R.drawable.renewable_resources, "Biking Increases \n Urban and Personal \n Health", "January 3,2018", "15%", "65%");
        temp.add(list_dataa);
        list_dataa = new TopPostListModel(R.drawable.net_neutrality, "Biking Increases \n Urban and Personal \n Health", "January 3,2018", "15%", "65%");
        temp.add(list_dataa);
        list.add(new CombineListModel(temp, true, true, false, true));

        CombineListModel list_data;

//recent
        list_data = new CombineListModel(new RelatPostModel(R.drawable.parison, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, true, false, "Recent Articles", "#05324f");
        list.add(list_data);

        list_data = new CombineListModel(new RelatPostModel(R.drawable.vote, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, false, false, "Recent Articles", "#05324f");
        list.add(list_data);

        list_data = new CombineListModel(new RelatPostModel(R.drawable.guns, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, false, true, "Recent Articles", "#05324f");
        list.add(list_data);
//society
        list_data = new CombineListModel(new RelatPostModel(R.drawable.mother_hood, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, true, false, "Society", "#981b1e");
        list.add(list_data);

        list_data = new CombineListModel(new RelatPostModel(R.drawable.generation, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, false, false, "Society", "#981b1e");
        list.add(list_data);

        list_data = new CombineListModel(new RelatPostModel(R.drawable.teaching, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, false, true, "Society", "#981b1e");
        list.add(list_data);
//foreign
        list_data = new CombineListModel(new RelatPostModel(R.drawable.korea, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, true, false, "Foreign\nPolicies", "#981b1e");
        list.add(list_data);

        list_data = new CombineListModel(new RelatPostModel(R.drawable.russia, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, false, false, "Foreign\nPolicies", "#981b1e");
        list.add(list_data);

        list_data = new CombineListModel(new RelatPostModel(R.drawable.japan, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, false, true, "Foreign\nPolicies", "#981b1e");
        list.add(list_data);
//domestic
        list_data = new CombineListModel(new RelatPostModel(R.drawable.mother_hood, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, true, false, "Domestic", "#981b1e");
        list.add(list_data);

        list_data = new CombineListModel(new RelatPostModel(R.drawable.police, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, false, false, "Domestic", "#981b1e");
        list.add(list_data);

        list_data = new CombineListModel(new RelatPostModel(R.drawable.guns, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, false, true, "Domestic", "#981b1e");
        list.add(list_data);

        //More Topic

        list_data = new CombineListModel(new TopicListModel(R.drawable.college, "college education neends to be more \naffordable", "January 3,2018", "15%", "15%", "#00000000"), true, true, false);
        list.add(list_data);

        list_data = new CombineListModel(new TopicListModel(R.drawable.banner, "college education neends to be more \naffordable", "January 3,2018", "15%", "15%", "#00000000"), true, false, true);
        list.add(list_data);
//education

        list_data = new CombineListModel(new RelatPostModel(R.drawable.teaching, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, true, false, "Education", "#981b1e");
        list.add(list_data);

        list_data = new CombineListModel(new RelatPostModel(R.drawable.college1, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, false, false, "Education", "#981b1e");
        list.add(list_data);

        list_data = new CombineListModel(new RelatPostModel(R.drawable.teaching2, "college education neends to be more affordable", "January 3,2018", "15%", "15%"), true, false, true, "Education", "#981b1e");
        list.add(list_data);
        combineAdapter.notifyDataSetChanged();
    }


}
