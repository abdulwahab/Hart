package com.example.wahab.hart.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.wahab.hart.R;
import com.example.wahab.hart.adapters.DoubleRecyclerViewAdapter;
import com.example.wahab.hart.models.DoubleRecyclerModel;

import java.util.ArrayList;
import java.util.List;


public class TopicPost extends Fragment {
    private List<DoubleRecyclerModel> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private DoubleRecyclerViewAdapter doubleAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_topic_post, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InitView(view);
        InitRecyclerView();
        listPostSocialData();
    }

    private void InitRecyclerView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(doubleAdapter);

    }

    private void InitView(View view) {
        recyclerView = view.findViewById(R.id.recyclerView_socialPost);
        recyclerView.setNestedScrollingEnabled(false);

        doubleAdapter = new DoubleRecyclerViewAdapter(list);
    }

    private void listPostSocialData() {
        list.clear();

        DoubleRecyclerModel list_data = new DoubleRecyclerModel(R.drawable.college, "college education neends to be more affordable", "January 3,2018", "15%", "65%", true, true, false);
        list.add(list_data);

        list_data = new DoubleRecyclerModel(R.drawable.banner, "college education neends to be more affordable", "January 3,2018", "15%", "65%", true, false, true);
        list.add(list_data);

        list_data = new DoubleRecyclerModel(false, R.drawable.parison, "college education neends to be more affordable", "Profit needs to End", "January 3,2018", "15%", "15%", true, false);
        list.add(list_data);

        list_data = new DoubleRecyclerModel(false, R.drawable.vote, "college education neends to be more affordable", "Profit needs to End", "January 3,2018", "15%", "15%", false, false);
        list.add(list_data);

        list_data = new DoubleRecyclerModel(false, R.drawable.guns, "college education neends to be more affordable", "Profit needs to End", "January 3,2018", "15%", "15%", false, false);
        list.add(list_data);

        list_data = new DoubleRecyclerModel(false, R.drawable.parison, "college education neends to be more affordable", "Profit needs to End", "January 3,2018", "15%", "15%", false, false);
        list.add(list_data);

        list_data = new DoubleRecyclerModel(false, R.drawable.guns, "college education neends to be more affordable", "Profit needs to End", "January 3,2018", "15%", "15%", false, false);
        list.add(list_data);

        list_data = new DoubleRecyclerModel(false, R.drawable.vote, "college education neends to be more affordable", "Profit needs to End", "January 3,2018", "15%", "15%", false, false);
        list.add(list_data);

        list_data = new DoubleRecyclerModel(false, R.drawable.parison, "college education neends to be more affordable", "Profit needs to End", "January 3,2018", "15%", "15%", false, false);
        list.add(list_data);

        list_data = new DoubleRecyclerModel(false, R.drawable.vote, "college education neends to be more affordable", "Profit needs to End", "January 3,2018", "15%", "15%", false, false);
        list.add(list_data);

        list_data = new DoubleRecyclerModel(false, R.drawable.guns, "college education neends to be more affordable", "Profit needs to End", "January 3,2018", "15%", "15%", false, true);
        list.add(list_data);

    }


}
