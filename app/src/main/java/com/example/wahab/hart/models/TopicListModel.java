package com.example.wahab.hart.models;

/**
 * Created by Wahab on 1/23/2018.
 */

public class TopicListModel {

    private String bg_color;
    private int topic_icon;
    private String topic_name;
    private String topic_date;
    private String like_count;
    private String dislike_count;

    public TopicListModel(int topic_icon, String topic_name, String topic_date, String like_count, String dislike_count, String bg_color) {
        this.topic_icon = topic_icon;
        this.topic_name = topic_name;
        this.topic_date = topic_date;
        this.like_count = like_count;
        this.dislike_count = dislike_count;
        this.bg_color = bg_color;
    }

    public int getTopic_icon() {
        return topic_icon;
    }

    public String getBg_color() {
        return bg_color;
    }

    public void setBg_color(String bg_color) {
        this.bg_color = bg_color;
    }

    public void setTopic_icon(int topic_icon) {
        this.topic_icon = topic_icon;
    }

    public String getTopic_name() {
        return topic_name;
    }

    public void setTopic_name(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getTopic_date() {
        return topic_date;
    }

    public void setTopic_date(String topic_date) {
        this.topic_date = topic_date;
    }

    public String getLike_count() {
        return like_count;
    }

    public void setLike_count(String like_count) {
        this.like_count = like_count;
    }

    public String getDislike_count() {
        return dislike_count;
    }

    public void setDislike_count(String dislike_count) {
        this.dislike_count = dislike_count;
    }
}
