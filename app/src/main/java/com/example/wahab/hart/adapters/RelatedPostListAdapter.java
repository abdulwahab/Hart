package com.example.wahab.hart.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.wahab.hart.R;
import com.example.wahab.hart.models.RelatPostModel;

import java.util.List;

/**
 * Created by Wahab on 1/23/2018.
 */

public class RelatedPostListAdapter extends RecyclerView.Adapter<RelatedPostListAdapter.MyViewHolder> {

    private List<RelatPostModel> List_view;

    public RelatedPostListAdapter(List<RelatPostModel> list_view) {
        List_view = list_view;
    }

    @Override
    public RelatedPostListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.related_post_list, parent, false);
        RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) itemView.getLayoutParams();
        lp.height = parent.getMeasuredHeight() / 3;
        itemView.setLayoutParams(lp);

        return new RelatedPostListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RelatedPostListAdapter.MyViewHolder holder, int position) {
        RelatPostModel frag = List_view.get(position);

        holder.postImg.setBackgroundResource(frag.getPost_Image());
        holder.post_name.setText(frag.getPost_heading());
        holder.post_date.setText(frag.getPost_date());
        holder.post_like.setText(frag.getPost_like());
        holder.post_dislike.setText(frag.getPost_dislike());

        holder.like_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.like_icon.getTag().equals("like")) {
                    holder.like_icon.setTag("unlike");
                    holder.like_icon.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_new));
                    String likeIncrement = holder.post_like.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value++;
                    holder.post_like.setText(value + "%");
                } else {
                    holder.like_icon.setTag("like");
                    holder.like_icon.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_icon1));
                    String likeIncrement = holder.post_like.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value--;
                    holder.post_like.setText(value + "%");
                }

            }
        });
        holder.dislike_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.dislike_icon.getTag().equals("dislike")) {
                    holder.dislike_icon.setTag("undislike");
                    holder.dislike_icon.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new2));
                    String likeIncrement = holder.post_dislike.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value++;
                    holder.post_dislike.setText(value + "%");
                } else {
                    holder.dislike_icon.setTag("dislike");
                    holder.dislike_icon.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new));
                    String likeIncrement = holder.post_dislike.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value--;
                    holder.post_dislike.setText(value + "%");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return List_view.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout postImg;
        public TextView post_name;
        public TextView post_date;
        public TextView post_like;
        public TextView post_dislike;
        public ImageView like_icon, dislike_icon;
        public LinearLayout like_view;
        public LinearLayout dislike_view;


        public MyViewHolder(View view) {
            super(view);

            postImg = view.findViewById(R.id.post_img);
            like_view = view.findViewById(R.id.like_view);
            post_name = view.findViewById(R.id.post_heading);
            dislike_view = view.findViewById(R.id.dislike_view);
            post_date = view.findViewById(R.id.post_date);
            post_like = view.findViewById(R.id.like_value);
            post_dislike = view.findViewById(R.id.dislike_value);

            //Like Dislike icon
            like_icon = view.findViewById(R.id.like_icon);
            dislike_icon = view.findViewById(R.id.dislike_icon);

        }
    }

}
