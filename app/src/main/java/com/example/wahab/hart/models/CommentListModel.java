package com.example.wahab.hart.models;

/**
 * Created by Wahab on 1/24/2018.
 */

public class CommentListModel {

    private String comment_userName;
    private String comment_time;
    private String comment;
    private String comment_likes;

    public CommentListModel(String comment_userName, String comment_time, String comment, String comment_likes) {
        this.comment_userName = comment_userName;
        this.comment_time = comment_time;
        this.comment = comment;
        this.comment_likes = comment_likes;
    }

    public String getComment_userName() {
        return comment_userName;
    }

    public void setComment_userName(String comment_userName) {
        this.comment_userName = comment_userName;
    }

    public String getComment_time() {
        return comment_time;
    }

    public void setComment_time(String comment_time) {
        this.comment_time = comment_time;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment_likes() {
        return comment_likes;
    }

    public void setComment_likes(String comment_likes) {
        this.comment_likes = comment_likes;
    }
}
