package com.example.wahab.hart.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.wahab.hart.R;
import com.example.wahab.hart.models.DoubleRecyclerModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wahab on 1/24/2018.
 */

public class DoubleRecyclerViewAdapter extends RecyclerView.Adapter<DoubleRecyclerViewAdapter.MyViewHolder> {

    private List<DoubleRecyclerModel> List_view = new ArrayList<>();
    View bottomLine;
    public boolean check;


    public DoubleRecyclerViewAdapter(List<DoubleRecyclerModel> list_view) {
        List_view = list_view;
    }

    @Override
    public DoubleRecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.related_item_view, parent, false);

        return new DoubleRecyclerViewAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DoubleRecyclerViewAdapter.MyViewHolder holder, int position) {
        DoubleRecyclerModel frag = List_view.get(position);

        if (frag.isCheck()) {
            holder.main_bg.setBackgroundColor(holder.main_bg.getContext().getResources().getColor(android.R.color.white));

            if (frag.isFirstPost()) {
                holder.title.setVisibility(View.VISIBLE);
                holder.title.setText("Top\n Voted");
            } else {
                holder.title.setVisibility(View.GONE);
            }
            if (frag.isLastPost()) {
                holder.read_more_topic.setVisibility(View.VISIBLE);
            } else {
                holder.read_more_topic.setVisibility(View.GONE);
            }
            holder.social_post.setVisibility(View.GONE);
            holder.topic_post.setVisibility(View.VISIBLE);
            holder.banner_Image.setBackgroundResource(frag.getBanner_Image());
            holder.post_name.setText(frag.getPost_heading());
            holder.post_date.setText(frag.getPost_date());
            holder.post_like.setText(frag.getPost_like());
            holder.post_dislike.setText(frag.getPost_dislike());

            holder.like_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.like.getTag().equals("like")) {
                        holder.like.setTag("unlike");
                        holder.like.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_new));
                        String likeIncrement = holder.post_like.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value++;
                        holder.post_like.setText(value + "%");
                    } else {
                        holder.like.setTag("like");
                        holder.like.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_icon1));
                        String likeIncrement = holder.post_like.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value--;
                        holder.post_like.setText(value + "%");
                    }

                }
            });
            holder.dislike_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.dislike.getTag().equals("dislike")) {
                        holder.dislike.setTag("undislike");
                        holder.dislike.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new2));
                        String likeIncrement = holder.post_dislike.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value--;
                        holder.post_dislike.setText(value + "%");
                    } else {
                        holder.dislike.setTag("dislike");
                        holder.dislike.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new));
                        String likeIncrement = holder.post_dislike.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value++;
                        holder.post_dislike.setText(value + "%");
                    }
                }
            });
//upr waly dono ko set krna hy nechy waly ko hide krna hay
        } else {
            holder.main_bg.setBackgroundColor(holder.main_bg.getContext().getResources().getColor(R.color.bg_feedPost));
            if (frag.isFirstSocial()) {
                holder.title.setVisibility(View.VISIBLE);
                holder.title.setText("Domestic Policy");
                holder.title.setTextColor(Color.parseColor("#2e2e2e"));
            } else {
                holder.title.setVisibility(View.GONE);
            }
            if (frag.isLoad()) {
                holder.loadmore.setVisibility(View.VISIBLE);
            } else {
                holder.loadmore.setVisibility(View.GONE);
            }

            holder.social_post.setVisibility(View.VISIBLE);
            holder.topic_post.setVisibility(View.GONE);
            holder.postImg.setBackgroundResource(frag.getPost_Image());
            holder.socail_heading.setText(frag.getSocail_heading());
            holder.socail_date.setText(frag.getSocail_date());
            holder.socail_like.setText(frag.getSocail_like());
            holder.socail_dislike.setText(frag.getSocail_dislike());
            //yha nechy walo ko sweet krna hy upr waly ko hide krna hay

            holder.like_view_social.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.like_social.getTag().equals("like")) {
                        holder.like_social.setTag("unlike");
                        holder.like_social.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_icon1));
                        String likeIncrement = holder.socail_like.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value--;
                        holder.socail_like.setText(value + "%");
                    } else {
                        holder.like_social.setTag("like");
                        holder.like_social.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_new));
                        String likeIncrement = holder.socail_like.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value++;
                        holder.socail_like.setText(value + "%");
                    }

                }
            });
            holder.dislike_view_social.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.dislike_social.getTag().equals("dislike")) {
                        holder.dislike_social.setTag("undislike");
                        holder.dislike_social.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new));
                        String likeIncrement = holder.socail_dislike.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value++;
                        holder.socail_dislike.setText(value + "%");
                    } else {
                        holder.dislike_social.setTag("dislike");
                        holder.dislike_social.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new2));
                        String likeIncrement = holder.socail_dislike.getText().toString().replace("%", "".trim());
                        int value = Integer.parseInt(likeIncrement);
                        value--;
                        holder.socail_dislike.setText(value + "%");
                    }
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return List_view.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //Top Voted Post
        public LinearLayout banner_Image, main_bg;
        public TextView post_name, post_main_heading;
        public TextView post_date;
        public TextView post_like;
        public TextView post_dislike;

        //Domestic Policy Post
        public RelativeLayout postImg, read_more_topic;
        public TextView socail_heading;
        public TextView socail_date;
        public TextView socail_like;
        public TextView socail_dislike, title, loadmore;
        public View topic_post, social_post;

        //Like Dislike
        LinearLayout like_view, dislike_view, like_view_social, dislike_view_social;
        ImageView like, dislike, like_social, dislike_social;


        public MyViewHolder(View view) {
            super(view);
            //main layout
            main_bg = view.findViewById(R.id.main_bg);

            bottomLine = view.findViewById(R.id.bottom_line);

            title = view.findViewById(R.id.title);
            read_more_topic = view.findViewById(R.id.read_more_topic);
            bottomLine.setVisibility(View.VISIBLE);
            loadmore = view.findViewById(R.id.load_more);
            banner_Image = view.findViewById(R.id.banner);
            post_name = view.findViewById(R.id.heading);
            post_date = view.findViewById(R.id.postTime);
            post_like = view.findViewById(R.id.post_like);
            post_dislike = view.findViewById(R.id.post_dislike);


            postImg = view.findViewById(R.id.post_img);
            socail_heading = view.findViewById(R.id.post_heading);
            post_main_heading = view.findViewById(R.id.post_main_heading);
            socail_date = view.findViewById(R.id.post_date);
            socail_like = view.findViewById(R.id.like_value);
            socail_dislike = view.findViewById(R.id.dislike_value);
            social_post = view.findViewById(R.id.social_post);
            topic_post = view.findViewById(R.id.topic_post);

            //Like dislike view
            like_view = view.findViewById(R.id.like_view_topic);
            like_view_social = view.findViewById(R.id.like_view_social);
            like = view.findViewById(R.id.like);
            like_social = view.findViewById(R.id.like_social);

            dislike_view = view.findViewById(R.id.dislike_view_topic);
            dislike_view_social = view.findViewById(R.id.dislike_view_social);
            dislike = view.findViewById(R.id.dislike);
            dislike_social = view.findViewById(R.id.dislike_social);


        }
    }
}
