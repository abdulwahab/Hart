package com.example.wahab.hart.network.model;


//import com.example.codingpixel.i_launched.data_structure.APIActions;

import com.example.wahab.hart.data_structure.APIActions;

public interface APIResponseListner {
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions);
    public void onRequestError(String response, APIActions.ApiActions apiActions);
}
