package com.example.wahab.hart.customUI.CustomFontET;

import android.content.Context;
import android.graphics.Typeface;
import android.text.util.Linkify;
import android.util.AttributeSet;

import com.example.wahab.hart.customUI.CustomET;
import com.example.wahab.hart.customUI.FontCache;

/**
 * Created by Wahab on 1/31/2018.
 */

public class CustomEditTextViewMonster extends CustomET {
    public CustomEditTextViewMonster(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public CustomEditTextViewMonster(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public CustomEditTextViewMonster(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("MontserratAlternates-Regular.otf", context);
        setTypeface(customFont, Typeface.NORMAL);
        setLinksClickable(true);
        setAutoLinkMask(Linkify.WEB_URLS);
    }

}
