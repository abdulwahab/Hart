package com.example.wahab.hart.interfaces;

/**
 * Created by codingpixel on 10/08/2017.
 */

public interface ReportButtonClickListner {
    void onReportClick(int position);
}
