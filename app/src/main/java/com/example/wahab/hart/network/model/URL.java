package com.example.wahab.hart.network.model;

public class URL {
    public static String getLocatioZipcode_part1 = "http://maps.google.com/maps/api/geocode/json?components=country%3a";
    public static String getLocatioZipcode_part2 = "%7Cpostal_code:";
    public static String getLocatioZipcode_part3 = "&sensor=true";
    public static String baseurl = "http://139.162.37.73/healingbudz/api/v1/";
    public static String images_baseurl = "http://139.162.37.73/healingbudz/public/images";
    public static String videos_baseurl = "http://139.162.37.73/healingbudz/public/videos";
    public static String check_email_availablity = "check_email";
    public static String get_defaults = baseurl + "get_defaults";
    public static String register = baseurl + "register";
    public static String login = baseurl + "login";
    public static String social_login = baseurl + "social_login";
    public static String refresh_user_data = baseurl + "get_user_session";

}
