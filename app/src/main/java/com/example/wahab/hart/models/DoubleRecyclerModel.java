package com.example.wahab.hart.models;

/**
 * Created by Wahab on 1/24/2018.
 */

public class DoubleRecyclerModel {

    private int banner_Image;
    private String post_heading;
    private String post_date;
    private String post_like;
    private String post_dislike;
    boolean isFirstPost;
    boolean isLastPost;


    private boolean check;

    private int post_Image;
    boolean isFirstSocial;
    private String socail_heading;
    private String socail_sub_heading;
    private String socail_date;
    private String socail_like;
    private String socail_dislike;

    boolean isLoad;

    public DoubleRecyclerModel(int banner_Image, String post_heading, String post_date, String post_like, String post_dislike, boolean check, int post_Image, String socail_heading, String socail_date, String socail_like, String socail_dislike) {
        this.banner_Image = banner_Image;
        this.post_heading = post_heading;
        this.post_date = post_date;
        this.post_like = post_like;
        this.post_dislike = post_dislike;
        this.check = check;
        this.post_Image = post_Image;
        this.socail_heading = socail_heading;
        this.socail_date = socail_date;
        this.socail_like = socail_like;
        this.socail_dislike = socail_dislike;
    }

    public DoubleRecyclerModel(int banner_Image, String post_heading, String post_date, String post_like, String post_dislike, boolean check, boolean isFirst, boolean isSecond) {
        this.banner_Image = banner_Image;
        this.post_heading = post_heading;
        this.post_date = post_date;
        this.post_like = post_like;
        this.post_dislike = post_dislike;
        this.check = check;
        this.isFirstPost = isFirst;
        this.isLastPost = isSecond;
    }

    public DoubleRecyclerModel(boolean check, int post_Image, String socail_heading, String socail_sub_heading, String socail_date, String socail_like, String socail_dislike, boolean isFirst, boolean load) {
        this.check = check;
        this.post_Image = post_Image;
        this.socail_heading = socail_heading;
        this.socail_sub_heading = socail_sub_heading;
        this.socail_date = socail_date;
        this.socail_like = socail_like;
        this.socail_dislike = socail_dislike;
        this.isFirstSocial = isFirst;
        this.isLoad = load;
    }

    public int getBanner_Image() {
        return banner_Image;
    }

    public void setBanner_Image(int banner_Image) {
        this.banner_Image = banner_Image;
    }

    public String getPost_heading() {
        return post_heading;
    }

    public void setPost_heading(String post_heading) {
        this.post_heading = post_heading;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getPost_like() {
        return post_like;
    }

    public void setPost_like(String post_like) {
        this.post_like = post_like;
    }

    public String getPost_dislike() {
        return post_dislike;
    }

    public void setPost_dislike(String post_dislike) {
        this.post_dislike = post_dislike;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public int getPost_Image() {
        return post_Image;
    }

    public void setPost_Image(int post_Image) {
        this.post_Image = post_Image;
    }

    public String getSocail_heading() {
        return socail_heading;
    }

    public void setSocail_heading(String socail_heading) {
        this.socail_heading = socail_heading;
    }

    public String getSocail_date() {
        return socail_date;
    }

    public void setSocail_date(String socail_date) {
        this.socail_date = socail_date;
    }

    public String getSocail_like() {
        return socail_like;
    }

    public void setSocail_like(String socail_like) {
        this.socail_like = socail_like;
    }

    public String getSocail_dislike() {
        return socail_dislike;
    }

    public void setSocail_dislike(String socail_dislike) {
        this.socail_dislike = socail_dislike;
    }

    public boolean isFirstPost() {
        return isFirstPost;
    }

    public void setFirstPost(boolean firstPost) {
        isFirstPost = firstPost;
    }

    public boolean isLastPost() {
        return isLastPost;
    }

    public void setLastPost(boolean lastPost) {
        isLastPost = lastPost;
    }

    public boolean isFirstSocial() {
        return isFirstSocial;
    }

    public void setFirstSocial(boolean firstSocial) {
        isFirstSocial = firstSocial;
    }

    public boolean isLoad() {
        return isLoad;
    }

    public void setLoad(boolean load) {
        isLoad = load;
    }

}
