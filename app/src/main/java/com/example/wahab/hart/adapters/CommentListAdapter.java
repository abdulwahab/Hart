package com.example.wahab.hart.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.wahab.hart.R;
import com.example.wahab.hart.models.CommentListModel;

import java.util.List;

/**
 * Created by Wahab on 1/24/2018.
 */

public class CommentListAdapter extends RecyclerView.Adapter<CommentListAdapter.MyViewHolder> {
    //    combine_list_adpater
    private List<CommentListModel> List_view;


    public CommentListAdapter(List<CommentListModel> list_view) {
        List_view = list_view;
    }

    @Override
    public CommentListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_list, parent, false);
        RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) itemView.getLayoutParams();
        lp.height = parent.getMeasuredHeight();
        itemView.setLayoutParams(lp);

        return new CommentListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CommentListAdapter.MyViewHolder holder, int position) {
        CommentListModel frag = List_view.get(position);

        holder.comment_userName.setText(frag.getComment_userName());
        holder.comment_time.setText(frag.getComment_time());
        holder.comment.setText(frag.getComment());
        holder.comment_like.setText(frag.getComment_likes());

        holder.like_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.clike.getTag().equals("like")) {
                    holder.clike.setTag("unlike");
                    holder.clike.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like1));
                    String likeIncrement = holder.comment_like.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value++;
                    holder.comment_like.setText(value + "%");
                } else {
                    holder.clike.setTag("like");
                    holder.clike.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like));
                    String likeIncrement = holder.comment_like.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value--;
                    holder.comment_like.setText(value + "%");
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return List_view.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView comment_userName;
        public TextView comment_time;
        public TextView comment;
        public TextView comment_like;
        LinearLayout like_view;
        ImageView clike;


        public MyViewHolder(View view) {
            super(view);

            comment_userName = view.findViewById(R.id.comment_userName);
            comment_time = view.findViewById(R.id.comment_time);
            comment = view.findViewById(R.id.comments);
            comment_like = view.findViewById(R.id.comment_like);

            //like comment
            like_view = view.findViewById(R.id.like_view);
            clike = view.findViewById(R.id.clike);

        }
    }
}
