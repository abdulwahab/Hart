package com.example.wahab.hart.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.wahab.hart.R;
import com.example.wahab.hart.models.TopPostListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wahab on 1/25/2018.
 */

public class GridAdapterView extends RecyclerView.Adapter<GridAdapterView.MyViewHolder> {
    private List<TopPostListModel> List_view = new ArrayList<>();


    public GridAdapterView(List<TopPostListModel> list_view) {
        List_view = list_view;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_item_view, parent, false);

        return new GridAdapterView.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        TopPostListModel frag = List_view.get(position);
        holder.cover_img.setBackgroundResource(frag.getTopPostImg());
        holder.cover_date.setText(frag.getTopPostDate());
        holder.cover_title.setText(frag.getTopPostHeadinh());
        holder.like_gv.setText(frag.getTopPostLike());
        holder.dislike_gv.setText(frag.getTopPostDislike());

        holder.like_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.like.getTag().equals("like")) {
                    holder.like.setTag("unlike");
                    holder.like.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_new));
                    String likeIncrement = holder.like_gv.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value++;
                    holder.like_gv.setText(value + "%");
                } else {
                    holder.like.setTag("like");
                    holder.like.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.like_icon1));
                    String likeIncrement = holder.like_gv.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value--;
                    holder.like_gv.setText(value + "%");
                }

            }
        });
        holder.dislike_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.dislike.getTag().equals("dislike")) {
                    holder.dislike.setTag("undislike");
                    holder.dislike.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new2));
                    String likeIncrement = holder.dislike_gv.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value--;
                    holder.dislike_gv.setText(value + "%");
                } else {
                    holder.dislike.setTag("dislike");
                    holder.dislike.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.dislike_new));
                    String likeIncrement = holder.dislike_gv.getText().toString().replace("%", "".trim());
                    int value = Integer.parseInt(likeIncrement);
                    value++;
                    holder.dislike_gv.setText(value + "%");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return List_view.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public View top_post, topic_post, social_post;
        public RecyclerView recyclerView_combine;

        //Top heading bar / common
        public TextView title;
        public LinearLayout top_like;

        //Top post
        public LinearLayout cover_img;
        public TextView cover_title, cover_date, like_gv, dislike_gv;

        //Recent ,society ,foreign ,domestic ,education
        public TextView post_heading, post_date, like_value, dislike_value;
        public RelativeLayout post_img, read_more_topic_common;

        //More Topics
        public LinearLayout banner;
        public TextView heading, postTime, post_like, post_dislike;
        public RelativeLayout read_more_topic;

        //Like Dislike Ids
        LinearLayout like_view, dislike_view;
        ImageView like, dislike;


        public MyViewHolder(View view) {
            super(view);
            //Top post or Gird view
            cover_img = view.findViewById(R.id.cover_img);
            cover_title = view.findViewById(R.id.cover_title);
            cover_date = view.findViewById(R.id.cover_date);
            like_gv = view.findViewById(R.id.like_gv);
            dislike_gv = view.findViewById(R.id.dislike_gv);

            //like dislike
            like_view = view.findViewById(R.id.like_view);
            like = view.findViewById(R.id.like);

            dislike_view = view.findViewById(R.id.dislike_view);
            dislike = view.findViewById(R.id.dislike);
        }
    }
}
