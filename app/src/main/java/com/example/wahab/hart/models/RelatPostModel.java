package com.example.wahab.hart.models;

/**
 * Created by Wahab on 1/23/2018.
 */

public class RelatPostModel {

    private int post_Image;
    private String post_heading;
    private String post_date;
    private String post_like;
    private String post_dislike;

    public RelatPostModel(int post_Image, String post_heading, String post_date, String post_like, String post_dislike) {
        this.post_Image = post_Image;
        this.post_heading = post_heading;
        this.post_date = post_date;
        this.post_like = post_like;
        this.post_dislike = post_dislike;
    }

    public int getPost_Image() {
        return post_Image;
    }

    public String getPost_heading() {
        return post_heading;
    }

    public String getPost_date() {
        return post_date;
    }

    public String getPost_like() {
        return post_like;
    }

    public String getPost_dislike() {
        return post_dislike;
    }

    public void setPost_Image(int post_Image) {
        this.post_Image = post_Image;
    }

    public void setPost_heading(String post_heading) {
        this.post_heading = post_heading;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public void setPost_like(String post_like) {
        this.post_like = post_like;
    }

    public void setPost_dislike(String post_dislike) {
        this.post_dislike = post_dislike;
    }
}
