package com.example.wahab.hart.models;

/**
 * Created by Wahab on 1/25/2018.
 */

public class TopPostListModel {

    private int topPostImg;
    private String topPostHeadinh;
    private String topPostDate;
    private String topPostLike;
    private String topPostDislike;

    public TopPostListModel(int topPostImg, String topPostHeadinh, String topPostDate, String topPostLike, String topPostDislike) {
        this.topPostImg = topPostImg;
        this.topPostHeadinh = topPostHeadinh;
        this.topPostDate = topPostDate;
        this.topPostLike = topPostLike;
        this.topPostDislike = topPostDislike;
    }

    public int getTopPostImg() {
        return topPostImg;
    }

    public void setTopPostImg(int topPostImg) {
        this.topPostImg = topPostImg;
    }

    public String getTopPostHeadinh() {
        return topPostHeadinh;
    }

    public void setTopPostHeadinh(String topPostHeadinh) {
        this.topPostHeadinh = topPostHeadinh;
    }

    public String getTopPostDate() {
        return topPostDate;
    }

    public void setTopPostDate(String topPostDate) {
        this.topPostDate = topPostDate;
    }

    public String getTopPostLike() {
        return topPostLike;
    }

    public void setTopPostLike(String topPostLike) {
        this.topPostLike = topPostLike;
    }

    public String getTopPostDislike() {
        return topPostDislike;
    }

    public void setTopPostDislike(String topPostDislike) {
        this.topPostDislike = topPostDislike;
    }
}
