package com.example.wahab.hart.customUI;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Wahab on 1/31/2018.
 */

public class CustomET extends android.support.v7.widget.AppCompatEditText {
    public CustomET(Context context) {
        super(context);
    }

    public CustomET(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomET(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
